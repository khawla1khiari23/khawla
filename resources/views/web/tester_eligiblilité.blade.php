<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">




<link type="image/png" rel="icon" href="{{ asset('/img/logo.png') }}" sizes="16x16">
<title>CPN</title>
<!-- javascripts -->
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src='https://cdn.rawgit.com/nizarmah/calendar-javascript-lib/master/calendarorganizer.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
{{-- script pour compteur--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
{{-- script pour compteur--}}
<!-- Styles -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
<link rel='stylesheet' href='https://cdn.rawgit.com/nizarmah/calendar-javascript-lib/master/calendarorganizer.min.css'>
<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/line-awesome/1.3.0/line-awesome/css/line-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/line-awesome/1.3.0/font-awesome-line-awesome/css/all.min.css">
<style>
  @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800|Poppins&display=swap');

  .carousel-indicators {
    background-color: red;


  }

  /* @media (max-width: 900px) {
        .services {
            display: flex;
            flex-direction: column;
        }
    } */
</style>

<style>
  .mySlides {
    display: none
  }

  .w3-left,
  .w3-right,
  .w3-badge {
    cursor: pointer
  }

  .w3-badge {
    height: 13px;
    width: 13px;
    padding: 0
  }

  .footer {
    width: 100%;

    height: 50%
  }
</style>

<body>



  <div class="w3-content w3-display-container" style="max-width:auto;display:block;">

    <div class="mySlides" style="width:100%">

      <div class="row">
        <div class="col-12" style="display: inherit;margin-top: 15%;">
          <div class="col-6">
            <h1>Nombre de salarié</h1>
            <select name="Salarie" id="salarie" style="border:#ce1212 solid 3px">
              <option value="de 0 à 5 Personnes" style=" padding-right: 10px;">de 0 à 5 Personnes</option>
              <option value="de 5 à 10 Personnes">de 5 à 10 Personnes</option>
              <option value="de 10 à 20 Personnes">de 10 à 20 Personnes</option>
              <option value="de 20 à 30 Personnes">de 20 à 30 Personnes</option>
              <option value="de 30 à 40 Personnes">de 30 à 40 Personnes</option>
              <option value="de 40 à 50 Personnes">de 40 à 50 Personnes</option>
              <option value="plus de 50 Personnes">plus de 50 Personnes</option>
            </select>
          </div>
          <div class="col-6">
            <img src="/public/img/imagebureau.jpg" alt="logo">
          </div>
        </div>
      </div>









    </div>

    <style>
      select {

        /* styling */
        background-color: white;
        border: thin solid rosybrown;
        border-radius: 4px;

        font: inherit;
        line-height: 1.5em;
        padding: 0.5em 3.5em 0.5em 1em;

        /* reset */

        margin: 0;

      }

      option:hover {
        color: rosybrown;
      }
    </style>
    <div class="mySlides" style="width:100%">
      <div class="row">
        <div class="col-12" style="display: inherit;margin-top: 15%;">
          <div class="col-6">

            <img src="/public/img/imagebureau.jpg" alt="logo">

          </div>
          <div class="col-6">
            <h1>Avez-vous un site internet </h1>
            <h1>pour votre entreprise ?</h1>
            <div style="margin: 15%;">
              <input type="radio" id="oui" name="oui" value="Oui" checked>
              <label for="oui">Oui</label>


              <input type="radio" id="non" name="non" value="Non">
              <label for="Non">Non</label>
            </div>
          </div>
        </div>

      </div>
    </div>



    <div class="mySlides" style="width:100%">
      <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
      <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
      <div class="row">
        <div class="col-12" style="display: inherit;margin-top: 15%;">
          <div class="col-6">

            <h1>Quel est votre </h1>
            <h1>type de site internet ?</h1>
            <div style="margin: 15%;">
              <input type="radio" id="oui" name="oui" value="Oui" checked>
              <label for="oui">E-commerce</label>


              <input type="radio" id="non" name="non" value="Non">
              <label for="Non">Vitrine</label>

              <input type="radio" id="non" name="non" value="Non">
              <label for="Non">Market-place</label>
            </div>
            <div class="col-md-4">
              <h5>Nombre de Vente</h5>
              <div class="input-group spinner">
                <input type="text" class="form-control" value="0" min="0" max="5000" style="border-color:#ce1212 ;">
                <div class="input-group-btn-vertical">
                  <button class="btn btn-default" type="button" style="background-color:#ce1212"><i style="color:white">+</i></button>
                  <button class="btn btn-default" type="button" style="background-color:#ce1212"><i style="color:white">-</i></button>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <h5>Nombre de Visite</h5>
              <div class="input-group spinner">
                <input type="text" class="form-control" value="+10 %" min="0" max="5000" style="border-color:#ce1212 ;">
                <div class="input-group-btn-vertical">
                  <button class="btn btn-default" type="button" style="background-color:#ce1212"><i style="color:white">+</i></button>
                  <button class="btn btn-default" type="button" style="background-color:#ce1212"><i style="color:white">-</i></button>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <h6>Nombre d'utilisateur</h6>
              <div class="input-group spinner">
                <input type="text" class="form-control" value="1" min="0" max="5" style="border-color:#ce1212">
                <div class="input-group-btn-vertical">
                  <button class="btn btn-default" type="button" style="background-color:#ce1212"><i style="color:white">+</i></button>
                  <button class="btn btn-default" type="button" style="background-color:#ce1212"><i style="color:white">-</i></button>
                </div>
              </div>
            </div>

            <style>
              @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);

              .spinner input {
                text-align: right;
              }

              .input-group-btn-vertical {
                position: relative;
                white-space: nowrap;
                width: 2%;
                vertical-align: middle;
                display: table-cell;
              }

              .input-group-btn-vertical>.btn {
                display: block;
                float: none;
                width: 100%;
                max-width: 100%;
                padding: 8px;
                margin-left: -1px;
                position: relative;
                border-radius: 0;
              }

              .input-group-btn-vertical>.btn:first-child {
                border-top-right-radius: 4px;
              }

              .input-group-btn-vertical>.btn:last-child {
                margin-top: -2px;
                border-bottom-right-radius: 4px;
              }

              .input-group-btn-vertical i {
                position: absolute;
                top: 0;
                left: 4px;
              }
            </style>
            <script>
              $(function() {

                $('.spinner .btn:first-of-type').on('click', function() {
                  var btn = $(this);
                  var input = btn.closest('.spinner').find('input');
                  if (input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                    input.val(parseInt(input.val(), 10) + 1);
                  } else {
                    btn.next("disabled", true);
                  }
                });
                $('.spinner .btn:last-of-type').on('click', function() {
                  var btn = $(this);
                  var input = btn.closest('.spinner').find('input');
                  if (input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                    input.val(parseInt(input.val(), 10) - 1);
                  } else {
                    btn.prev("disabled", true);
                  }
                });

              })
            </script>
          </div>
          <div class="col-6">
            <img src="/public/img/imagebureau.jpg" alt="logo">
          </div>

        </div>
      </div>

    </div>
  </div>



  <div class="mySlides" style="width:100%">
    <div class="row">
      <div class="col-12" style="display: inherit;margin-top: 10%;">
        <div class="col-6">
          <h1 style="margin: -25px 63px 21px 277px;">Date de</h1>
          <h1 style="margin: 0 0 0 270px;">developpement ?</h1>
          <img src="/public/img/c.jpg" alt="calender" style="margin: 30px 16px 15px 553px;">
        </div>
        <div class="col-6">
          <select name="Salarie" id="salarie" style="border:#ce1212 solid 3px;width:500px">
            <option value="" style=" padding-right: 10px;"></option>
            <option value="">Avant 2000</option>
            <option value="">Années 2000-2003</option>
            <option value="">Années 2003-2006</option>
            <option value="">Années 2006-2009</option>
            <option value="">Années 2009-2012</option>
            <option value="">Années 2012-2015</option>
            <option value="">Années 2015-2018</option>
            <option value="">Années 2018-2020</option>
          </select>
        </div>
      </div>
    </div>




  </div>
  </div>



  <div class="mySlides" style="width:100%">
    <div class="row">
      <div class="col-12" style="display: inherit;margin-top: 10%;">
        <div class="col-6">

        </div>
        <div class="col-6">
          <h4 style="margin: 0 -0px 0px 187px;"> Service </h4>



          <div class="wrapper">
            <div class="searchBar" style="margin: -83px 0 0 68px;">
              <input id="searchQueryInput" type="text" name="searchQueryInput" placeholder="|" value="" />
              <button id="searchQuerySubmit" type="submit" name="searchQuerySubmit">
                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                  <path fill="#666666" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                </svg>
              </button>
            </div>


            <br>

            <table class="text-left w-full" style="border:#ce1212 solid 3px ;">
              <tbody class="bg-grey-light flex flex-col items-center justify-between overflow-y-scroll w-full" style="height: 20vh; width: 300vh;">
                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 300px;">
                    <h5>Vidéo motion design</h5>
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="VMD" name="VMD"></td>

                </tr>
                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4">
                    <h5> Vidéo marketing</h5>
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="VM" name="VM"></td>

                </tr>
                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class=" w-1/4">photo</td>
                  <td class=" w-1/4" style="width: 300%">
                    <h5> Community management </h5>
                    <h6> (pack standart)</h6>
                  </td>
                  <td class=" w-1/4"><input type="radio" id="CM" name="CM"></td>

                </tr>
                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 100%">
                    <h5>Community management</h5>
                    <h6> (pack master)</h6>
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="CMP" name="CMP"></td>

                </tr>

              </tbody>
            </table>
          </div>

          <style>
            .wrapper {
              width: 100%;
              max-width: 31.25rem;
              margin: 6rem auto;
            }

            .label {
              font-size: .625rem;
              font-weight: 400;
              text-transform: uppercase;
              letter-spacing: +1.3px;
              margin-bottom: 1rem;
            }

            .searchBar {
              width: 100%;
              display: flex;
              flex-direction: row;
              align-items: center;
            }

            #searchQueryInput {
              width: 100%;
              height: 2.8rem;
              background: #f5f5f5;
              outline: none;
              border: none;
              border-radius: 1.625rem;
              padding: 0 3.5rem 0 1.5rem;
              font-size: 1rem;
            }

            #searchQuerySubmit {
              width: 3.5rem;
              height: 2.8rem;
              margin-left: -3.5rem;
              background: none;
              border: none;
              outline: none;
            }

            #searchQuerySubmit:hover {
              cursor: pointer;
            }
          </style>


        </div>
      </div>
    </div>
  </div>

  <div class="mySlides" style="width:100%">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <style>
      @import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap');

      * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Poppins', sans-serif;
      }


      .wrapper .icon {
        margin: 0 20px;
        text-align: center;
        cursor: pointer;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        position: relative;
        z-index: 2;
      }

      .wrapper .icon span {
        display: block;
        height: 60px;
        width: 60px;
        background: #fff;
        border-radius: 50%;
        position: relative;
        z-index: 2;
      }

      .wrapper .icon span i {
        line-height: 60px;
        font-size: 25px;
      }

      .wrapper .icon .tooltip {
        position: absolute;
        top: 0;
        z-index: 1;
        background: gray;
        color: gray;
        padding: 10px 18px;
        font-size: 20px;
        font-weight: 500;
        border-radius: 25px;
        opacity: 0;
        pointer-events: none;
      }

      .wrapper .icon:hover .tooltip {
        top: -70px;
        opacity: 1;
        pointer-events: auto;
      }

      .icon .tooltip:before {
        position: absolute;
        content: "";
        height: 15px;
        width: 15px;
        background: gray;
        left: 50%;
        bottom: -6px;
        transform: translateX(-50%) rotate(45deg);
      }

      .wrapper .icon:hover span {
        color: #000;
      }

      .wrapper .icon:hover span,
      .wrapper .icon:hover .tooltip {
        color: #000;
        font-size: 12px;
      }

      .wrapper .hover-me:hover span,
      .wrapper .hover-me:hover .tooltip,
      .wrapper .hover-me:hover .tooltip:before {
        background: #000;
      }
    </style>
    <div class="row">
      <div class="col-12" style="display: inherit;margin-top: 10%;">
        <div class="col-6">

        </div>
        <div class="col-6">
          <h4 style="margin: 0 -0px 0px 187px;"> Service </h4>



          <div class="wrapper">
            <div class="searchBar" style="margin: -83px 0 0 68px;">
              <input id="searchQueryInput" type="text" name="searchQueryInput" placeholder="|" value="" />
              <button id="searchQuerySubmit" type="submit" name="searchQuerySubmit">
                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                  <path fill="#666666" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                </svg>
              </button>
            </div>


            <br>

            <table class="text-left w-full" style="border:#ce1212 solid 3px ;">
              <tbody class="bg-grey-light flex flex-col items-center justify-between overflow-y-scroll w-full" style="height: 20vh; width: 300vh;">
                <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 300px;">
                    <h5>Création logo</h5>
                  </td>
                  <div class="wrapper">
                    <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
                      <div class="tooltip">
                        Facebook
                      </div>
                  </div>
                  </td>
          </div>

          </tr>

          <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
            <td class="p-1 w-1/4">photo</td>
            <td class="p-1 w-1/4">
              <h5> Refonte logo</h5>
            </td>
            <div class="wrapper">
              <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
                <div class="tooltip">
                  Facebook
                </div>
            </div>
            </td>
        </div>
        </tr>
        <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
          <td class=" w-1/4">photo</td>
          <td class=" w-1/4" style="width: 300%">
            <h5> Carte de visite </h5>

          </td>
          <div class="wrapper">
            <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
              <div class="tooltip">
                Facebook
              </div>
          </div>
          </td>
      </div>
      </tr>
      <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
        <td class="p-1 w-1/4">photo</td>
        <td class="p-1 w-1/4" style="width: 100%">
          <h5>Flyer commerciale</h5>
        </td>
        <div class="wrapper">
          <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
            <div class="tooltip">
              Facebook
            </div>
        </div>
        </td>
    </div>
    </tr>
    <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
      <td class="p-1 w-1/4">photo</td>
      <td class="p-1 w-1/4" style="width: 100%">
        <h5>Carte de fidélité</h5>
      </td>
      <div class="wrapper">
        <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
          <div class="tooltip">
            Facebook
          </div>
      </div>
      </td>
  </div>
  </tr>


  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>Poster / Affiche </h5>
    </td>
    <style>
      .hover-on:hover {
        background-color: coral;
      }
    </style>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>
    </td>
    </div>
  </tr>


  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>Dépliant</h5>
    </td>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>
    </td>
    </div>
  </tr>


  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>plaquette</h5>
    </td>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>
    </td>
    </div>
  </tr>


  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>Retouche photo</h5>
    </td>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>
    </td>
    </div>
  </tr>


       </tbody>
    </table>

     </div>
    </div>
   </div>
   </div>
 </div>


  <div class="mySlides" style="width:100%">
    <div class="row">
      <div class="col-12" style="display: inherit;margin-top: 10%;">
        <div class="col-6">

        </div>
         <div class="col-6">
          <h4 style="margin: 0 -0px 0px 187px;"> Service </h4>



             <div class="wrapper">
              <div class="searchBar" style="margin: -83px 0 0 68px;">
              <input id="searchQueryInput" type="text" name="searchQueryInput" placeholder="|" value="" />
                <button id="searchQuerySubmit" type="submit" name="searchQuerySubmit">
                  <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                    <path fill="#666666" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                 </svg>
                </button>
            </div>


            <br>

            <table class="text-left w-full" style="border:#ce1212 solid 3px ;">
              <tbody class="bg-grey-light flex flex-col items-center justify-between overflow-y-scroll w-full" style="height: 20vh; width: 300vh;">
                <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 300px;">
                    <h5>Charte graphique</h5>
                  </td>
                  <div class="wrapper">
                    <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
                      <div class="tooltip">
                        Facebook
                      </div>
                  </div>
                  </td>
          </div>

          </tr>

          <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
            <td class="p-1 w-1/4">photo</td>
            <td class="p-1 w-1/4">
              <h5>Signature mail</h5>
            </td>
            <div class="wrapper">
              <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
                <div class="tooltip">
                  Facebook
                </div>
            </div>
            </td>
        </div>
        </tr>
        <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
          <td class=" w-1/4">photo</td>
          <td class=" w-1/4" style="width: 300%">
            <h5> Menu restaurant </h5>

          </td>
          <div class="wrapper">
            <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
              <div class="tooltip">
                Facebook
              </div>
          </div>
          </td>
      </div>
      </tr>
      <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
        <td class="p-1 w-1/4">photo</td>
        <td class="p-1 w-1/4" style="width: 100%">
          <h5>Cover single</h5>
        </td>
        <div class="wrapper">
          <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
            <div class="tooltip">
              Facebook
            </div>
        </div>
        </td>
    </div>
    </tr>
    <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
      <td class="p-1 w-1/4">photo</td>
      <td class="p-1 w-1/4" style="width: 100%">
        <h5>Pochette a rabat</h5>
      </td>
      <div class="wrapper">
        <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
          <div class="tooltip">
            Facebook
          </div>
      </div>
      </td>
  </div>
  </tr>


  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>Catalogue </h5>
    </td>
    <style>
      .hover-on:hover {
        background-color: coral;
      }
    </style>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>
    </td>
    </div>
  </tr>


  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>Couverture de livre</h5>
    </td>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>
    </td>
    </div>
  </tr>


  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>Kit stand salon</h5>
    </td>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>
    </td>
    </div>
  </tr>


  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>Bon de commande</h5>
    </td>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>

    
    </td>
    </div>
  </tr>

  <tr class="flex w-full mb-4 hover-on" style="border:#ce1212 solid 3px ;">
    <td class="p-1 w-1/4">photo</td>
    <td class="p-1 w-1/4" style="width: 100%">
      <h5>Packaging</h5>
    </td>
    <div class="wrapper">
      <td class="p-1 w-1/4 icon facebook"><input type="radio" id="CMP" name="CMP">
        <div class="tooltip">
          Facebook
        </div>
    </div>

    
    </td>
    </div>
  </tr>

       </tbody>
    </table>

     </div>
    </div>
   </div>
   </div>



  </div>
  <div class="mySlides" style="width:100%">
  <div class="mySlides" style="width:100%">
    <div class="row">
      <div class="col-12" style="display: inherit;margin-top: 10%;">
        <div class="col-6">

        </div>
        <div class="col-6">
          <h4 style="margin: 0 -0px 0px 187px;"> Service </h4>



          <div class="wrapper">
            <div class="searchBar" style="margin: -83px 0 0 68px;">
              <input id="searchQueryInput" type="text" name="searchQueryInput" placeholder="|" value="" />
              <button id="searchQuerySubmit" type="submit" name="searchQuerySubmit">
                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                  <path fill="#666666" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                </svg>
              </button>
            </div>


            <br>

            <table class="text-left w-full" style="border:#ce1212 solid 3px ;">
              <tbody class="bg-grey-light flex flex-col items-center justify-between overflow-y-scroll w-full" style="height: 20vh; width: 300vh;">
                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 300px;">
                    <h5>Site Marketplace</h5>
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="VMD" name="VMD"></td>

                </tr>
                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4">
                    <h5> Site web réferencement de Lea</h5>
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="VM" name="VM"></td>

                </tr>
                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class=" w-1/4">photo</td>
                  <td class=" w-1/4" style="width: 300%">
                    <h5>Site vitrine </h5>
                   
                  </td>
                  <td class=" w-1/4"><input type="radio" id="CM" name="CM"></td>

                </tr>
                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 100%">
                    <h5>Site e-commerce</h5>
                  
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="CMP" name="CMP"></td>
                  </tr>


                  <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 100%">
                    <h5>ERP/ERP</h5>
                  
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="CMP" name="CMP"></td>

                </tr>

                <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 100%">
                    <h5>Application réseaux sociaux</h5>
                  
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="CMP" name="CMP"></td>
                 </tr>
                 <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 100%">
                    <h5>Application Market place</h5>
                  
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="CMP" name="CMP"></td>
                 </tr>

                 <tr class="flex w-full mb-4" style="border:#ce1212 solid 3px ;">
                  <td class="p-1 w-1/4">photo</td>
                  <td class="p-1 w-1/4" style="width: 100%">
                    <h5>Application Jeux mobile</h5>
                  
                  </td>
                  <td class="p-1 w-1/4"><input type="radio" id="CMP" name="CMP"></td>
                 </tr>


              </tbody>
            </table>
         </div>
       </div>
      </div>
    </div>
  </div>
  </div>
  <div class="mySlides" style="width:100%">
    <h1 style="color: #111D5E;font-weight: bold;">Lien du site25 ?</h1>
  </div>
  <div class="mySlides" style="width:100%">
    <h1 style="color: #111D5E;font-weight: bold;">Lien du site26 ?</h1>
  </div>
  <div class="mySlides" style="width:100%">
    <h1 style="color: #111D5E;font-weight: bold;">Lien du site27 ?</h1>
  </div>
  <div class="mySlides" style="width:100%">
    <h1 style="color: #111D5E;font-weight: bold;">Lien du site28 ?</h1>
  </div>
  <div class="mySlides" style="width:100%">
    <h1 style="color: #111D5E;font-weight: bold;">Lien du site29 ?</h1>
  </div>
  <div class="mySlides" style="width:100%">
    <h1 style="color: #111D5E;font-weight: bold;">Lien du site30 ?</h1>
  </div>




  </div>


  <script>
    var slideIndex = 1;
    showDivs(slideIndex);

    function plusDivs(n) {
      showDivs(slideIndex += n);
    }

    function currentDiv(n) {
      showDivs(slideIndex = n);
    }

    function showDivs(n) {
      var i;
      var x = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("demo");
      if (n > x.length) {
        slideIndex = 1
      }
      if (n < 1) {
        slideIndex = x.length
      }
      for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
      }
      for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" w3-white", "");
      }
      x[slideIndex - 1].style.display = "block";
      dots[slideIndex - 1].className += " w3-white";
    }
  </script>

  <style>
    .w3-border {
      border: 1px solid #000 !important
    }
  </style>

  <footer style="background: white;bottom:0;width:100%;" class="primary_footer">
    <div class="container p-5">
      <div class="row justify-content-center">

        <div class="col-md-9 col-xl-9 col-xs-12 d-flex justify-content-center">
          <div class="w3-left w3-hover-text-khaki" style="margin-left: -210px;padding: inherit;color:#000" onclick="plusDivs(-1)">&#10094;</div>

          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(3)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(4)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(5)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(6)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(7)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(8)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(9)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(10)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(11)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(12)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(13)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(14)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(15)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(16)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(17)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(18)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(19)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(20)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(21)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(22)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(23)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(24)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(25)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(26)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(27)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(28)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(29)"></span>
          <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(30)"></span>
          <div class="w3-right w3-hover-text-khaki" style="margin-left: 9px;color: black;font-weight: bold;" onclick="plusDivs(1)">&#10095;</div>


        </div>
      </div>
    </div>
  </footer>

  <footer style="background: #111D5E;" class="primary_footer">
    <div class="container p-5">
      <div class="row g-0 ">
        <div class="col-md-4 mb-3 ">
          <img class="mb-3 brand_logo d-inline-block align-text-top" src="{{asset('/img/logo-cpn3.png')}}" alt="" width="" height="45">
          <p style="color:white;font-size: 13px;">Le Cabinet de Propulsion Numérique aide les entreprises à se propulser numériquement et à bénéficier de financement.
            CPN est un organisme de financement à but non lucratif</p>
          <ul style="margin:0; padding:0; display: flex; flex-direction:row">
            <li style="list-style: none;"><a href="#"><img style="width:40px; height:40px" src="{{asset('/img/facebook.gif')}}" alt=""></a></li>
            <li style="list-style: none;margin:0 5px"><a href="#"><img style="width:40px; height:40px" src="{{asset('/img/instagram.png')}}" alt=""></a></li>
            <li style="list-style: none;margin:0 5px 0 0"><a href="#"><img style="width:40px; height:40px" src="{{asset('/img/twitter.png')}}" alt=""></a></li>
            <li style="list-style: none;"><a href="#"><img style="width:40px; height:40px" src="{{asset('/img/youtube.png')}}" alt=""></a></li>
          </ul>
        </div>
        <div class="col-md-8">
          <div class="row g-3">
            <div class="col-md-4 py-2 px-4 d-flex flex-column align-items-center justify-content-center" style="">
              <h3 style="color: white; font-size: 20px; font-weight: 600;">Réseau sociaux</h3> <br>
              <ul style="width:100%; margin:0;padding:0;margin-left: 139px;font-size: 15px;">
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Instagram</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Youtube</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Linkedin</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Facebook</a></li>
              </ul>
            </div>
            <div class="col-md-4 py-2 px-4 d-flex flex-column align-items-center justify-content-center">
              <h3 style="color: white; font-size: 20px; font-weight: 600;">Support</h3> <br>
              <ul style="width:100%; margin:0;padding:0;margin-left: 139px;font-size: 15px;">
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">FAQ</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Inscription</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Actualité</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Contact</a></li>
              </ul>
            </div>
            <div class="col-md-4 py-2 px-4 d-flex flex-column align-items-center justify-content-center" style="    margin-top: -47px;">
              <h3 style="color: white; font-size: 20px; font-weight: 600;">Contact</h3> <br>
              <ul style="width:100%; margin:0;padding:0;margin-left: 139px;font-size: 15px; ">
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">+33 6 73 46 65 64</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">votreconseiller@cpn-aide-aux-entreprise.com</a></li>

              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>








</body>