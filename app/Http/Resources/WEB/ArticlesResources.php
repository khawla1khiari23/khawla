<?php

namespace App\Http\Resources\WEB;
use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticlesResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "title"=>$this->title,
            "desc"=>$this->desc,
            "created_at"=>$this->parsedate($this->created_at),
            "verified_at"=>$this->verified_at,
        ];
    }

    public function parsedate($date){
        $date = new DateTime($this->date);
        $date = $date->format("d F");
        /* $date = str_replace( 
            array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"),
            array("Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"),
            $date
        ); */
        $date = str_replace(
            array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
            array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'),
            $date
        );

        return $date;

    }
}
