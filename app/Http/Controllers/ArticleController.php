<?php

namespace App\Http\Controllers;
use App\Models\Article;
use App\Models\Picture;
use Illuminate\Http\Request;
use App\Http\Resources\WEB\ArticlesResources;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
class ArticleController extends Controller
{
  public function getarticles(){
   
    $articles=DB::table("articles")->join("pictures","articles.id","articles_id")->orderby("created_at","desc")->limit(3)->get();

    $article2=DB::table("articles")->join("pictures","articles.id","articles_id")->limit(10)->get();
    $articles = ArticlesResources::collection($articles);

    return view("web.actuality",["articles"=>$articles,"article2"=>$article2]);
  }


  //Get article by region
  public function getarticle($region){
    $company_id = "76078573";
    $client = new Client();
    $res = $client->request('GET', 'https://www.linkedin.com/pages-extensions/FollowCompany?id=' . $company_id . '&counter=bottom');

    $html = strval($res->getBody());
    $tag = "<div class=\"follower-count\">";
    $pos = strpos($html, $tag) + strlen($tag);
    $result = "";
    while ($html[$pos] != "<") {
      $result .= $html[$pos];
      $pos++;
    }
    
    $pourcentage = $result * 7 / 100;
    $articles=DB::table("articles")
    ->join("pictures","articles.id","articles_id")
    ->join("regions","articles.id","regions.id_article")
    ->where("regions.name",$region)
    ->limit(3)->get();
    $count=DB::table("articles")
    ->join("pictures","articles.id","articles_id")
    ->join("regions","articles.id","regions.id_article")
    ->where("regions.name",$region)
    ->limit(3)->count();

  
    $message="Cette region n'as pas des actualitées pour le moment";

    return view("web.home",["result"=>$result,"pourcent"=>$pourcentage,"message"=>$message,"count"=>$count,"articles"=>$articles]);
  }
 

 
}
