<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Exception;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login_page()
    {
        return view('web.connexion.login');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        $company_id = "76078573";
        $client = new Client();
        $res = $client->request('GET', 'https://www.linkedin.com/pages-extensions/FollowCompany?id=' . $company_id . '&counter=bottom');

        $html = strval($res->getBody());
        $tag = "<div class=\"follower-count\">";
        $pos = strpos($html, $tag) + strlen($tag);
        $result = "";

        while ($html[$pos] != "<") {
            $result .= $html[$pos];
            $pos++;
        }
        $pourcentage = $result * 7 / 100;
        try {

            $user = Socialite::driver('google')->user();
            $finduser = User::where('email', $user->email)->first();
            if ($finduser) {
                return view("web.home", ['result' => $result, 'pourcent' => $pourcentage]);
            } else {
                return redirect()->back()->with(['error2' => "Vous n'etez pas registrer!"]);
            }
        } catch (Exception $e) {
        }
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();
    }

    public function redirectToLinkedin()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    public function handleLinkedinCallback()
    {
        $user = Socialite::driver('linkedin')->user();
    }





    public function trait_login(Request $request)
    {
        request()->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        $company_id = "76078573";
        $client = new Client();
        $res = $client->request('GET', 'https://www.linkedin.com/pages-extensions/FollowCompany?id=' . $company_id . '&counter=bottom');

        $html = strval($res->getBody());
        $tag = "<div class=\"follower-count\">";
        $pos = strpos($html, $tag) + strlen($tag);
        $result = "";

        while ($html[$pos] != "<") {
            $result .= $html[$pos];
            $pos++;
        }
        $pourcentage = $result * 7 / 100;


        $user = User::where('email', $request->email)->get()->first();
        if ($user->email_verified_at == null) {
            return redirect()->back()->with(['error1' => 'Il faux vérifier votre compte']);
        } else {
            $resultat = auth()->attempt([
                'email' => request('email'),
                'password' => request('password'),
            ]);
$count=0;
$message="Cliquer sur votre région pour obtenir votre actualité";
            if ($resultat) {
                if (auth()->guest()) {
                    return view("web.home",['result'=>$result,'pourcent'=>$pourcentage,"count"=>$count,"message"=>$message]);
                } else {
                    $articles = DB::table("articles")->join("pictures", "articles.id", "articles_id")->limit(3)->get();

                    return view("web.home", ["articles" => $articles,'result'=>$result,'pourcent'=>$pourcentage,"count"=>$count,"message"=>$message]);
                }
            } else {
                return redirect()->back()->with(['error' => 'Verifier vos informations !']);
            }
        }
    }
}
