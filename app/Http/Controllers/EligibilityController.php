<?php

namespace App\Http\Controllers;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;

class EligibilityController extends Controller
{
    public function index()
    {
        return view("pages.eligibility");
    }
}
