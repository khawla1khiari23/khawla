<?php

namespace App\Http\Controllers;

use App\Mail\MailComment;
use App\Mail\SignupEmail;
use App\Models\Comment;
use App\Models\Contact;
use App\Models\Page;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
  public function index()
  {

    return view("web.home");
  }


  public function linkedin_f()
  {

    $company_id = "76078573";
    $client = new Client();
    $res = $client->request('GET', 'https://www.linkedin.com/pages-extensions/FollowCompany?id=' . $company_id . '&counter=bottom');

    $html = strval($res->getBody());
    $tag = "<div class=\"follower-count\">";
    $pos = strpos($html, $tag) + strlen($tag);
    $result = "";

    while ($html[$pos] != "<") {
      $result .= $html[$pos];
      $pos++;
    }
    return $result;
  }


  public function home()
  {

   $result=$this->linkedin_f();

      $pourcentage = $result * 7 / 100;
  
      $count=0;
      $message="Cliquer sur votre région pour obtenir votre actualité";



      $avis=DB::table("contacts")->orderby("created_at","desc")->limit(3)->get();
  
      if(auth()->guest())
     {      $articles = DB::table("articles")->join("pictures", "articles.id", "articles_id")->limit(3)->get();

        return view("web.home", ["articles" => $articles, "result" => $result, "pourcent" => $pourcentage,"count"=>$count,"message"=>$message,"avis"=>$avis]);
     }else{
      $articles = DB::table("articles")->join("pictures", "articles.id", "articles_id")->limit(3)->get();
      return view("web.home", ["articles" => $articles, "result" => $result, "pourcent" => $pourcentage,"count"=>$count,"message"=>$message]);
    }
  }
 public function send_comment(Request $request)
  {
    $commentt = $request->comment;
    if (auth()->guest()) {
      return view('web.connexion.login');
    } else {
      $comment = new Contact();
      $comment->avis = $commentt;
      $comment->users_id = auth()->user()->id;
      $comment->first_name= auth()->user()->first_name;
      $comment->first_name= auth()->user()->last_name;
      $comment->email= auth()->user()->email;
      $comment->phone= auth()->user()->phone;
      $comment->save();
      $request->session()->put('comment', $commentt);
      Mail::send(new MailComment());
      return redirect()->back()->with(['success' => 'Nous avons recu votre avis!']);
    }
  }

  public function Entreprise(Request $request)
  {
    $result=$this->linkedin_f();

    $pourcentage = $result * 7 / 100;
 return view('web.Entreprise', [ "result" => $result, "pourcent" => $pourcentage]);
  }
  public function Agence(Request $request)
  { $result=$this->linkedin_f();

    $pourcentage = $result * 7 / 100;
    $count=0;
    $message="Cliquer sur votre région pour obtenir votre actualité";
    return view('web.Agence', [ "result" => $result, "pourcent" => $pourcentage,"count"=>$count,"message"=>$message]);
  }
  public function Collectivites(Request $request)
  {$result=$this->linkedin_f();

    $pourcentage = $result * 7 / 100;
    $count=0;
    $message="Cliquer sur votre région pour obtenir votre actualité";
    return view('web.Collectivites', [ "result" => $result, "pourcent" => $pourcentage,"count"=>$count,"message"=>$message]);
  }
  
}
