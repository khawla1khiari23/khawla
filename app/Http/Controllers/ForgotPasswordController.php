<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
   public function forgot_password(Request $request)
   {
  $user=User::where('email',$request->email)->get()->first();

if($user==null)
{
    return redirect()->back()->with(['error' => "Ce email n'existe pas !"]);
}
else
{
    Mail::send('web.mail.Forgotpassword_mail', ['email' => $user->email], function ($message) use ($request, $user) {
        $message->to($user->email)
            ->from('votreconseiller@cpn-aide-aux-entreprise.com')
            ->subject("Mot de passe oublier");
          });
          return redirect()->back()->with(['success' => "Nous avous vous envoyer un lien de réinitialisation , veuillez vérifier votre boite email"]);
    }
   }
 





}