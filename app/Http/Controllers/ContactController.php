<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Mail\MailComment;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contact(Request $request)
    {
        if ($request->accept == null)
        return redirect()->back()->with(['error1' => "Il faut accepter les CGU et la Politque de site"]);
        else {
        $contact = new Contact();
        $contact->first_name = $request->nom;
        $contact->last_name = $request->prenom;
        $contact->email = $request->email;
        $contact->comment = $request->message;
        $contact->phone = $request->phone;
        $contact->save();
        $request->session()->put('message',$request->message);

        Mail::send(new ContactMail());

        return redirect()->back()->with(['success' => 'Merci de Nous Contactez!']);
        }
    }
}
