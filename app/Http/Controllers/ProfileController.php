<?php

namespace App\Http\Controllers;

use App\Models\Addresses;
use App\Models\Picture;
use App\Models\Social_account;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function profile_page()
    {
        //
        if (auth()->guest())
            return view('web.connexion.login');
        else
            $avatar = Picture::where('users_id', Auth()->user()->id)->get()->first();
        //    $account=Social_account::where('user_id',Auth()->user()->id)->get()->first();
            $adresse = Addresses::where('users_id', Auth()->user()->id)->get()->first();

        return view('web.connexion.profile', ['adress' => $adresse, 'avatar' => $avatar]);
    }

    public function deconnexion()
    {
        auth()->logout();
        return redirect('/');
    }
    public function edit_profile_page()
    {
        if (auth()->guest())
            return view('web.connexion.login');
        else
            //            $account=Social_account::where('user_id',Auth()->user()->id)->get()->first();
            $adresse = Addresses::where('users_id', Auth()->user()->id)->get()->first();
            $avatar = Picture::where('users_id', Auth()->user()->id)->get()->first();
        return view('web.connexion.edit_profile', ['adress' => $adresse, 'avatar' => $avatar]);
    }

    public function updt_profile(Request $request)
    {


        $user = User::findOrFail(Auth()->user()->id);

        $user->first_name = $request->first_name;
        $user->email = $request->email;
        $user->phone= $request->tel;
        $user->save();

        $adresse = Addresses::where('users_id', Auth()->user()->id)->get()->first();
        $adresse->address = $request->adresse;
        $adresse->save();

        $picture=Picture::where('users_id', Auth()->user()->id)->get()->first();
        $picture->name=$request->image;
        $picture->save();

        //        $social_account=Social_account::where('user_id',Auth()->user()->id)->get()->first();
        //        $social_account->website=$request->website;
        //        $social_account->facebook=$request->facebook;
        //        $social_account->twitter=$request->twitter;
        //        $social_account->instagram=$request->instagram;
        //        $social_account->save();

        if ($request->hasFile('image')) {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('images', $filename, 'public');
            $picture->update(['name' => $filename]);
        }
        return redirect()->back()->with(['success' => 'Profile updated successfully!']);
    }
}
