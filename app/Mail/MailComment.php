<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailComment extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->with("mailers.smtp")
            ->from('Avis@cpn-aide-aux-entreprises.com',"cpn-aide-aux-entreprises")
            ->to("ghaziarfaa@gmail.com")
            ->bcc("s.smida@jobid.fr")
            ->subject("Avis ,Client")
            ->markdown('web.mail.coments_email');
    }
}
