<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
  body  {
        margin:0;
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .container{
        width: 90%;
        display: grid;
        grid-template-columns: repeat(auto-fit,minmax(250px,1fr));
     grid-gap: 20px;


    }
    .box{
        height: 200px;
        color: white;
        border: 2px solid white;
        position: relative;

    }
    h2{
        text-transform: uppercase;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        margin: 0;
        position:absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
    }
</style>
<body>
    <div class="container">
        <div class="box">
            <h2>Box 1</h2>
        </div>
        <div class="box">
            <h2>Box 2</h2>
        </div>
        <div class="box">
            <h2> Box 3</h2>
        </div>
    </div>
</body>
</html>