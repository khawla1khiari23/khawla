<?php

use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;

use App\Mail\EligibilityMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\VerfiedController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'prevent-back-history'], function () {

    Route::domain("localhost")->group(function () {

        Route::prefix("mail")->group(function () {
            Route::get("confirm", [MailController::class, "confirmMeet"]);
            Route::get("change", [MailController::class, "changeMeet"]);
        });

//les regions (subvention)
       
            Route::get("Normandie", [RegionController::class, "Normandie_page"])->name('Normandie');
           
    
//les regions (subvention)





        Route::get("", function () {
            return view("web.index");
        });
        

        Route::get('/home',[\App\Http\Controllers\HomeController::class,'home'])->name('home');

        Route::get('/getarticle/{region}',[\App\Http\Controllers\ArticleController::class,'getarticle'])->name('/getarticle/region');

       
        Route::get('actuality', [\App\Http\Controllers\ArticleController::class, 'getarticles'])->name('actuality');
        
        Route::get("agenda", function () {
            return view("web.agenda");
        })->name('agenda');

        Route::get("calendar", function () {
            return view("web.calendar");
        })->name('calendar');

        Route::get("contact", function () {
            return view("web.contact");
        })->name('contact');

        Route::get("about", function () {
           return view('web.about_us');
        })->name('about');
//    Login Route
        Route::get("login", 'App\Http\Controllers\LoginController@login_page')->name('login');
        Route::post("connexion_trait", 'App\Http\Controllers\LoginController@trait_login')->name('connexion_trait');
//    Login Route
//Register Route
        Route::get("registre", 'App\Http\Controllers\RegisterController@reg_page')->name('registre');
        Route::post("registre_trait", 'App\Http\Controllers\RegisterController@trait_reg')->name('registre_trait');
//Register Route

//    Subvention Route
        Route::get('subevention', function () {
            return view('web.subvention');
        })->name('subevention');

//    Subvention Route


        //Google login
        Route::get('login/google', [\App\Http\Controllers\LoginController::class, 'redirectToGoogle'])->name('login.google');
        Route::get('login/google/callback', [\App\Http\Controllers\LoginController::class, 'handleGoogleCallback']);
        //Google login

        //facebook login
        Route::get('login/facebook', [\App\Http\Controllers\LoginController::class, 'redirectToFacebook'])->name('login.facebook');
        Route::get('login/facebook/callback', [\App\Http\Controllers\LoginController::class, 'handleFacebookCallback']);
        //facebook login

        //linkedin login
        Route::get('login/linkedin', [\App\Http\Controllers\LoginController::class, 'redirectToLinkedin'])->name('login.linkedin');
        Route::get('login/linkedin/callback', [\App\Http\Controllers\LoginController::class, 'handleLinkedinCallback']);
        //linkedin login

        //Profile Page
        Route::get('profile', [ProfileController::class, 'profile_page'])->name('profile');
        //Profile Page
        Route::post('contact',[\App\Http\Controllers\ContactController::class,'contact'])->name('contact');

        //EditProfile
        Route::get('/edit_profile',[ProfileController::class,'edit_profile_page']);
        Route::post('updt_profile',[ProfileController::class,'updt_profile'])->name('updt_profile');
        //EditProfile

        Route::post('send_comment',[\App\Http\Controllers\HomeController::class,'send_comment'])->name('send_comment');


        Route::get('testemail',[VerfiedController::class,"sendSignupEmail"]);

        //déconnexion
        Route::get('deconnexion', [ProfileController::class, 'deconnexion'])->name('déconnexion');
        //déconnexion

        // About us
        Route::get('a_propos', function () {
            return view('web.about_us');
        })->name('a_propos');
        // About us

        Route::get('map', function () {
            return view('web.map');
        });

        //popup
        Route::get('pop',function ()
        {
            return view('web.popup');
        });
        //popup

        //forgot password
            Route::get('mot_passe_oubliee',function (){
               return view('web.connexion.forgot_p');
            })->name('mot_passe_oubliee');

            Route::post('forgot_password', [ForgotPasswordController::class, 'forgot_password'])->name('forgot_password');

        //forgot password


        //stiky 
        Route::get('Entreprise', [HomeController::class, 'Entreprise'])->name('Entreprise');
        Route::get('Agence', [HomeController::class, 'Agence'])->name('Agence');
        Route::get('Collectivites', [HomeController::class, 'Collectivites'])->name('Collectivites');
///lien tester votre eligibiliter de page home
        Route::get('Tester_votre_eligibilité',function (){
            return view('web.tester_eligiblilité');
         })->name('Tester_votre_eligibilité');

         //lien en savoir plus (accompagniment) page home
         Route::get('En_savoir_plus',function (){
            return view('web.ensavoirplus_accomp');
         })->name('En_savoir_plus');


           //lien en savoir plus (financement) page home
           Route::get('En_savoir_plus_f',function (){
            return view('web.ensavoirplus_finance');
         })->name('En_savoir_plus_f');
    });
});

Route::domain('crm.localhost')->group(function () {
    Route::get('/{any}', function () {
        return view('crm.app');
    })->where('any', '.*');
});

/* Route::get('/{any}', function(){
    return view('app');
})->where('any','.*'); */

Route::get("/test/email", function () {
    //Mail::send(new EligibilityMail());
    //return new EligibilityMail();
});
